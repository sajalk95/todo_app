var express = require('express');
var router = express.Router();
var UserModel = require("../models/users");
var display = false;

/* GET users listing. */
router.get('/signup', function(req, res, next) {
  res.render('signup');
});

router.post('/signup', async (req, res)=>{
  try{
    let user = new UserModel(req.body);
    //await user.save()
    display = true;
    module.exports.display = display;
    res.redirect('/')
  }catch(err){
    res.send(err)
  }
});

module.exports.router = router;
